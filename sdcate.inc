<?php
/**
 * @file
 * Helper functions for writing DCAT.
 */

module_load_include('inc', 'sdcate', 'sdcate.ns');
module_load_include('inc', 'sdcate', 'sdcate.scrape');

/**
 * Get the URI of the DCAT Catalog.
 *
 * @return string
 *   String respresentation of DCAT Catalog URI.
 */
function sdcate_uri_catalog() {
  $cat = &drupal_static(__FUNCTION__);
  if (!isset($cat)) {
    $cat = variable_get_value('sdcate_uri') . '/catalog';
  }
  return $cat;
}

/**
 * Get the URI of a DCAT Dataset.
 *
 * @param int $did
 *   Dataset ID, typically the node id.
 *
 * @return string
 *   String respresentation of DCAT dataset URI.
 */
function sdcate_uri_dataset($did) {
  return sdcate_uri_catalog() . '/dataset/' . $did;
}

/**
 * Get the URI of a DCAT Distribution.
 *
 * @param int $fid
 *   Distribution ID, typically the file id.
 *
 * @return string
 *   String respresentation of DCAT distribution URI.
 */
function sdcate_uri_distribution($fid) {
  return sdcate_uri_catalog() . '/distribution/' . $fid;
}


/**
 * Generate DCAT Catalog.
 *
 * @return string
 *   DCAT Catalog as RDF triples.
 */
function sdcate_catalog() {
  global $base_url;

  $sdcat = new SdcateCatalog();
  $sdcat->uri = sdcate_uri_catalog();
  $sdcat->homepage = $base_url;
  if ($name = variable_get_value('site_name')) {
    $sdcat->title[LANGUAGE_NONE] = $name;
  }
  if ($slogan = variable_get_value('site_slogan')) {
    $sdcat->desc[LANGUAGE_NONE] = $slogan;
  }
  if ($geo = variable_get_value('sdcate_geo')) {
    $sdcat->geo = $geo;
  }
  return $sdcat;
}


/**
 * Generate a DCAT Dataset.
 *
 * @param string $cat
 *   URI of the DCAT Catalog.
 * @param object $node
 *   Node to be exported.
 *
 * @return as string
 *   DCAT Dataset as RDF triples.
 */
function sdcate_dataset($cat, $node) {
  $dset = sdcate_uri_dataset($node->nid);
  
  $sdset = new SdcateDataset();
  $sdset->uri = $dset;
  if ($node->language != LANGUAGE_NONE) {
    $sdset->lang = SDCATE_LOC . $node->language;
  }
  $sdset->title[$node->language] = $node->title;
  $sdset->issued = $node->created;
  $sdset->modified = $node->changed;
  $sdset->distributions = sdcate_distributions($dset, $node);

  return $sdset;
}

/**
 * Generate DCAT Datasets.
 *
 * @return string
 *   DCAT Datasets as RDF triples.
 */
function sdcate_datasets() {
  $dsets = array();

  $cat = sdcate_uri_catalog();
  $nids = sdcate_nodes_list();

  foreach ($nids as $id) {
    $nodes = entity_load('node', array($id));
    if (isset($nodes) && !empty($nodes)) {
      $node = array_pop($nodes);
      // Check if anonymous user has access to this node.
      if (node_access('view', $node, 0)) {
        $dsets[] = sdcate_dataset($cat, $node);
      }
    }
  }
  return $dsets;
}

/**
 * Generate a DCAT Distribution.
 *
 * @param string $dset
 *   URI of the DCAT Dataset.
 * @param array $file
 *   File to be exported.
 *
 * @return string
 *   DCAT Distribution as RDF triples.
 */
function sdcate_distribution($dset, $file) {
  $sdist = new SdcateDistribution();
  $sdist->uri = sdcate_uri_distribution($file['fid']);
  $sdist->title = $file['filename'];
  if (isset($file['description']) && !empty($file['description'])) {
    $sdist->desc = $file['description'];
  }
  $sdist->issued = $file['timestamp'];
  $sdist->size = $file['filesize'];
  $sdist->media = $file['filemime'];
  $sdist->download = file_create_url($file['uri']);

  return $sdist;
}

/**
 * Check if file is public and is a machine-friendly file format.
 *
 * @param array $file
 *   File to be checked.
 *
 * @return bool
 *   TRUE if a file is to public and n a open format.
 */
function sdcate_file_check($file) {
  $check = FALSE;

  // Check if file is public
  if ((substr($file['uri'], 0, 9) == 'public://') && ($file['display'] == 1)) { 
    // Check file extension
    if ($pos = strrpos($file['uri'], '.')) {
      $type = substr($file['uri'], $pos + 1);
      if ($type) {
        $types = explode(' ', variable_get_value('sdcate_files'));
        $check = in_array(strtolower($type), $types);
      }
    }
  }
  return $check;
}

/**
 * Generate DCAT Distributions.
 *
 * @param string $dset
 *   URI of the DCAT Dataset.
 * @param object $node
 *   Node to be exported.
 *
 * @return string
 *   DCAT Distributions as RDF triples.
 */
function sdcate_distributions($dset, $node) {
  $dists = array();

  // Get the list of file fields.
  $fields = sdcate_filefields_list();
  foreach ($fields[$node->type] as $field) {
    if (isset($node->{$field}) && !empty($node->{$field})) {
      foreach (array_keys($node->{$field}) as $lang) {
        foreach ($node->{$field}[$lang] as $file) {
          if (sdcate_file_check($file)) {
            $dists[] = sdcate_distribution($dset, $file);
          }
        }
      }
    }
  }
  return $dists;
}
