<?php
/**
 * @file
 * Various namespace constants.
 */

define('SDCATE_LOC', 'http://id.loc.gov/vocabulary/iso639-1/');
define('SDCATE_RDF', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#');
define('SDCATE_XSD', 'http://www.w3.org/2001/XMLSchema#');
define('SDCATE_DCAT', 'http://www.w3.org/ns/dcat#');
define('SDCATE_DT', 'http://purl.org/dc/terms/');
define('SDCATE_FOAF', 'http://xmlns.com/foaf/0.1/');
