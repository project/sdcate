<?php
/**
 * @file
 * Helper classes.
 */

/**
 * Generic class.
 */
class SdcateGeneric {
  public $uri = '';
  public $title = array();
  public $desc = array();
  public $lang = array();
  public $issued = 0;
  public $modified = 0;
}

/**
 * DCAT Catalog class.
 */
class SdcateCatalog extends SdcateGeneric {
  public $homepage = '';
  public $license = '';
  public $rights = '';
  public $spatial = '';
  public $datasets = array();
} 

/**
 * DCAT Dataset class.
 */
class SdcateDataset extends SdcateGeneric {
  public $keyword = array();
  public $page = array();
  public $contact = '';
  public $spatial = '';
  public $temporal = '';
  public $accrual = '';
  public $distributions = array();
}

/**
 * DCAT Distribution class.
 */
class SdcateDistribution extends SdcateGeneric {
  public $license = '';
  public $rights = '';
  public $access = '';
  public $download = '';
  public $media = '';
  public $format = '';
  public $size = 0;
}
