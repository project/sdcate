<?php
/**
 * @file
 * Helper functions for writing DCAT.
 */

module_load_include('inc', 'sdcate', 'lib/sdcate.dcat');
module_load_include('inc', 'sdcate', 'lib/sdcate.ns');

define('SDCATE_RDF_TYPE', SDCATE_RDF . 'type');

define('SDCATE_XSD_DTIME', SDCATE_XSD . 'dateTime');
define('SDCATE_XSD_DEC', SDCATE_XSD . 'decimal');

define('SDCATE_DCAT_ACAT', SDCATE_DCAT . 'Catalog');
define('SDCATE_DCAT_ADSET', SDCATE_DCAT . 'Dataset');
define('SDCATE_DCAT_ADIST', SDCATE_DCAT . 'Distribution');
define('SDCATE_DCAT_DSET', SDCATE_DCAT . 'dataset');
define('SDCATE_DCAT_DIST', SDCATE_DCAT . 'distribution');

define('SDCATE_DCAT_CONTACT', SDCATE_DCAT . 'contactPoint');
define('SDCATE_DCAT_PAGE', SDCATE_DCAT . 'landingPage');
define('SDCATE_DCAT_TAG', SDCATE_DCAT . 'keyword');
define('SDCATE_DCAT_THEME', SDCATE_DCAT . 'theme');
define('SDCATE_DCAT_URL', SDCATE_DCAT . 'downloadURL');
define('SDCATE_DCAT_SIZE', SDCATE_DCAT . 'byteSize');
define('SDCATE_DCAT_TYPE', SDCATE_DCAT . 'mediaType');

define('SDCATE_DT_CREAT', SDCATE_DT . 'issued');
define('SDCATE_DT_DESC', SDCATE_DT . 'description');
define('SDCATE_DT_GEO', SDCATE_DT . 'spatial');
define('SDCATE_DT_LANG', SDCATE_DT . 'language');
define('SDCATE_DT_MODIF', SDCATE_DT . 'modified');
define('SDCATE_DT_TITLE', SDCATE_DT . 'title');

define('SDCATE_FOAF_DOC', SDCATE_FOAF . 'Document');
define('SDCATE_FOAF_HOME', SDCATE_FOAF . 'homepage');

class SdcateTriplesSerializer {
  /**
   * Creates RDF triple.
   *
   * @param string $subj
   *   Subject of this triple as URI.
   * @param string $pred
   *   Predicate of this triple as URI.
   * @param string $obj
   *   Object of this triple as URI.
   *
   * @return string
   *   String representation of the triple.
   */
  protected static function triple($subj, $pred, $obj) {
    return "<$subj> <$pred> <$obj> . \n";
  }

  /**
   * Create RDF triple.
   *
   * @param string $subj
   *   Subject of this triple as URI.
   * @param string $pred
   *   Predicate of this triple as URI.
   * @param string $lit
   *   String to be added as object.
   * @param string $lang
   *   Optional language identifier.
   *
   * @return string
   *   String representation of the triple.
   */
  protected static function literal($subj, $pred, $lit, $lang = LANGUAGE_NONE) {
    $obj = '"' . addcslashes($lit, "\\\'\"\n\r") . '"';
    if (isset($lang) && !empty($lang) && ($lang != LANGUAGE_NONE)) {
      $obj .= '@' . $lang;
    }
    return "<$subj> <$pred> $obj .\n";
  }

  /**
   * Create RDF triple.
   *
   * @param string $subj
   *   Subject of this triple as URI.
   * @param string $pred
   *   Predicate of this triple as URI.
   * @param string $stamp
   *   Timestamp (in seconds since epoch).
   *
   * @return string
   *   String representation of the triple.
   */
  protected static function datetime($subj, $pred, $stamp) {
    $obj = '"' . date(DATE_ISO8601, $stamp) . '"';
    $obj .= '^^<' . SDCATE_XSD_DTIME . '>';
    return "<$subj> <$pred> $obj .\n";
  }

  /**
   * Create RDF triple.
   *
   * @param string $subj
   *   Subject of this triple as URI.
   * @param string $pred
   *   Predicate of this triple as URI.
   * @param string $decimal
   *   Decimal as string.
   *
   * @return string
   *   String representation of the triple.
   */
  protected static function decimal($subj, $pred, $decimal) {
    $obj = '"' . $decimal . '"';
    $obj .= '^^<' . SDCATE_XSD_DEC . '>';
    return "<$subj> <$pred> $obj .\n";
  }

  /**
   * Serializes a catalog.
   *
   * @param SdcateCatalog $cat
   *   DCAT Catalog.
   *
   * @return string
   *   String representation of the catalog.
   */
  public function catalog(SdcateCatalog $cat) {
    $data = $this::triple($cat->uri, SDCATE_RDF_TYPE, SDCATE_DCAT_ACAT);
    $data .= $this::triple($cat->uri, SDCATE_FOAF_HOME, $cat->homepage);
    foreach($cat->title as $lang => $title) {
      $data .= $this::literal($cat->uri, SDCATE_DT_TITLE, $title, $lang);
    }
    foreach($cat->desc as $lang => $desc) {
      $data .= $this::literal($cat->uri, SDCATE_DT_DESC, $desc, $lang);
    }
    if (isset($cat->geo) && !empty($cat->geo)) {
      $data .= $this::triple($cat->uri, SDCATE_DT_GEO, $cat->geo);
    }
    return $data;
  }

  /**
   * Serializes a dataset.
   *
   * @param SdcateDataset $dset
   *   DCAT Dataset.
   * @param string $caturi
   *   URI of the Catalog this Dataset belongs to.
   *
   * @return string
   *   String representation of the dataset. 
   */
  public function dataset(SdcateDataset $dset, $caturi) {
    $data = $this::triple($caturi, SDCATE_DCAT_DSET, $dset->uri);
    $data .= $this::triple($dset->uri, SDCATE_RDF_TYPE, SDCATE_DCAT_ADSET);
    foreach($dset->title as $lang => $title) {
      $data .= $this::triple($dset->uri, SDCATE_DT_LANG, $dset->lang);
      $data .= $this::literal($dset->uri, SDCATE_DT_TITLE, $title, $lang);
    }
    $data .= $this::datetime($dset->uri, SDCATE_DT_CREAT, $dset->issued);
    $data .= $this::datetime($dset->uri, SDCATE_DT_MODIF, $dset->modified);
    foreach($dset->distributions as $dist) {
      $data .= $this::distribution($dist, $dset->uri);
    }

    return $data;
  }

  /**
   * Serializes a dsitribution.
   *
   * @param SdcateDistribution $dist
   *   DCAT Distribution.
   * @param string $dseturi
   *   URI of the Dataset this Distribution belongs to.
   *
   * @return string
   *   String representation of the distribution. 
   */
  public function distribution(SdcateDistribution $dist, $dseturi) {
    $data = $this::triple($dseturi, SDCATE_DCAT_DIST, $dist->uri);
    $data .= $this::triple($dist->uri, SDCATE_RDF_TYPE, SDCATE_DCAT_ADIST);
    $data .= $this::literal($dist->uri, SDCATE_DT_TITLE, $dist->title);
    if (isset($dist->desc) && !empty($dist->desc)) {
      $data .= $this::literal($dist->uri, SDCATE_DT_DESC, $dist->desc);
    }
    $data .= $this::datetime($dist->uri, SDCATE_DT_CREAT, $dist->issued);
    $data .= $this::decimal($dist->uri, SDCATE_DCAT_SIZE, $dist->size);
    $data .= $this::literal($dist->uri, SDCATE_DCAT_TYPE, $dist->media);
    $data .= $this::triple($dist->uri, SDCATE_DCAT_URL, $dist->download);

    return $data;
  }
}
